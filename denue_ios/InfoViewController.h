//
//  InfoViewController.h
//  inegiDENUE
//
//  Created by Jorge Ernesto Mauricio on 11/4/12.
//  Copyright (c) 2012 Jorge Ernesto Mauricio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>
{
    AppDelegate *appDelegate;
}

@property (strong, nonatomic) IBOutlet UIPickerView *picker;
@property (strong, nonatomic) NSMutableArray *arrayNumbers;

@end
