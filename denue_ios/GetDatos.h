//
//  GetDatos.h
//  DENUE
//
//  Created by Jorge Mauricio on 9/2/12.
//  Copyright (c) 2012 Jorge Mauricio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Datos.h"

@protocol GetDatosDelegate <NSObject>

-(void)returnResponse;

@end

@interface GetDatos : NSObject <NSXMLParserDelegate>
{
    BOOL waitingForEntryTitle;
    id delegate;
    AppDelegate *appDelegate;
    NSXMLParser *parser;
    Datos *datos;
}

@property (strong, nonatomic) id<GetDatosDelegate>delegate;
@property (strong, nonatomic) NSMutableData *xmlData;
@property (strong, nonatomic) NSMutableString *titleString;
@property (strong, nonatomic) NSURLConnection *connectionInProgress;
@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) NSString *xmlToPost;
@property (strong, nonatomic) NSString *responseXML;
@property (strong, nonatomic) NSString *stringCondicion;
@property (strong, nonatomic) NSMutableString *condicionString;

@property (strong, nonatomic) NSString *auxEntidad;
@property (strong, nonatomic) NSString *auxMunicipio;

// Arrays

@property (strong, nonatomic) NSMutableArray *arrayString;

-(void)startConnection;
-(NSString*)getPath;

@end

