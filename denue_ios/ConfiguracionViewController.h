//
//  ConfiguracionViewController.h
//  inegiDENUE
//
//  Created by Jorge Ernesto Mauricio on 11/25/12.
//  Copyright (c) 2012 Jorge Ernesto Mauricio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfoViewController.h"
#import "EntidadViewController.h"

@interface ConfiguracionViewController : UITableViewController
{
    AppDelegate *appDelegate;
    InfoViewController *infoView;
    EntidadViewController *entidadView;
}

@property (strong, nonatomic) UISwitch *buttonSwitch;
@property (strong, nonatomic) UISegmentedControl *segmentedControl;

@end
