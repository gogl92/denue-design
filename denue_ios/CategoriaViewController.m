//
//  CategoriaViewController.m
//  inegiDENUE
//
//  Created by Jorge Ernesto Mauricio on 11/2/12.
//  Copyright (c) 2012 Jorge Ernesto Mauricio. All rights reserved.
//

#import "CategoriaViewController.h"
#import "MapViewController.h"
#import "ResultadosViewController.h"

@interface CategoriaViewController ()

@end

@implementation CategoriaViewController
@synthesize tabBarController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Categorías";
    }
    return self;
}

-(IBAction)cafeteria:(id)sender
{
    appDelegate.id_categoria = @"722219";
    appDelegate.tipoBusqueda = @"5";
    [self changeView];
}
-(IBAction)restaurant:(id)sender
{
    appDelegate.id_categoria = @"7221";
    appDelegate.tipoBusqueda = @"5";
    [self changeView];
}
-(IBAction)hospital:(id)sender
{
    appDelegate.id_categoria = @"622";
    appDelegate.tipoBusqueda = @"5";
    [self changeView];
}
-(IBAction)abarrote:(id)sender
{
    appDelegate.id_categoria = @"461110";
    appDelegate.tipoBusqueda = @"5";
    [self changeView];
}
-(IBAction)parque:(id)sender
{
    appDelegate.id_categoria = @"71311";
    appDelegate.tipoBusqueda = @"5";
    [self changeView];
}
-(IBAction)bar:(id)sender
{
    appDelegate.id_categoria = @"722412";
    appDelegate.tipoBusqueda = @"5";
    [self changeView];
}
-(IBAction)antro:(id)sender
{
    appDelegate.id_categoria = @"722411";
    appDelegate.tipoBusqueda = @"5";
    [self changeView];
}
-(IBAction)banco:(id)sender
{
    appDelegate.id_categoria = @"522110";
    appDelegate.tipoBusqueda = @"5";
    [self changeView];
}
-(IBAction)escuela:(id)sender
{
    appDelegate.id_categoria = @"61";
    appDelegate.tipoBusqueda = @"5";
    [self changeView];
}
-(IBAction)hotel:(id)sender
{
    appDelegate.id_categoria = @"72111";
    appDelegate.tipoBusqueda = @"5";
    [self changeView];
}
-(IBAction)gasolinera:(id)sender
{
    appDelegate.id_categoria = @"468411";
    appDelegate.tipoBusqueda = @"5";
    [self changeView];
}
-(IBAction)centroComercial:(id)sender
{
    appDelegate.id_categoria = @"531311";
    appDelegate.tipoBusqueda = @"5";
    [self changeView];
}

-(void)changeView
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        
        mapViewController = [[MapViewController alloc]
                             initWithNibName:@"MapViewController" bundle:nil];
        resultadosView = [[ResultadosViewController alloc] initWithStyle:UITableViewStyleGrouped];
        tabBarController = [[UITabBarController alloc] init];
        tabBarController.viewControllers = @[mapViewController, resultadosView];
        [self.navigationController pushViewController:tabBarController animated:YES];
    }else
    {
        mapViewController = [[MapViewController alloc]
                             initWithNibName:@"MapViewController_iPad" bundle:nil];
        [self.navigationController pushViewController:mapViewController animated:YES];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    appDelegate.radioBusqueda = @"500";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
