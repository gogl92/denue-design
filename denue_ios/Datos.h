//
//  Datos.h
//  DENUE
//
//  Created by Jorge Mauricio on 9/2/12.
//  Copyright (c) 2012 Jorge Mauricio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Datos : NSObject

@property (strong, nonatomic) NSString *idDato;
@property (strong, nonatomic) NSString *nombreUnidadEconomica;
@property (strong, nonatomic) NSString *razonSocial;
@property (strong, nonatomic) NSString *codigoClase;
@property (strong, nonatomic) NSString *nombreClase;
@property (strong, nonatomic) NSString *personalOcupado;
@property (strong, nonatomic) NSString *tipoVialidad;
@property (strong, nonatomic) NSString *nombreVialidad;
@property (strong, nonatomic) NSString *numeroExterior;
@property (strong, nonatomic) NSString *edificio;
@property (strong, nonatomic) NSString *numeroInterior;
@property (strong, nonatomic) NSString *nombreAsentamiento;
@property (strong, nonatomic) NSString *corredorIndustrial;
@property (strong, nonatomic) NSString *numeroLocal;
@property (strong, nonatomic) NSString *codigoPostal;
@property (strong, nonatomic) NSString *entidadFederativa;
@property (strong, nonatomic) NSString *municipio;
@property (strong, nonatomic) NSString *localidad;
@property (strong, nonatomic) NSString *areaGeoestadistica;
@property (strong, nonatomic) NSString *manzana;
@property (strong, nonatomic) NSString *telefono;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *website;
@property (strong, nonatomic) NSString *tipoUnidadEconomica;
@property (strong, nonatomic) NSString *latitud;
@property (strong, nonatomic) NSString *longitud;
@property (strong, nonatomic) NSString *fechaIncorporacion;

@end
