//
//  DetailViewController.m
//  DENUE
//
//  Created by Jorge Mauricio on 9/2/12.
//  Copyright (c) 2012 Jorge Mauricio. All rights reserved.
//

#import "DetailViewController.h"
#import "Datos.h"

@interface DetailViewController ()

@end

@implementation DetailViewController
@synthesize objetoSeleccionado;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.title = @"Detalles";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 27;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
    }
    // Configure the cell...
    
    Datos *datos = [appDelegate.arrayInfo objectAtIndex:[objetoSeleccionado intValue]];
    
    if (indexPath.row == 0) {
        cell.textLabel.text = @"ID";
        cell.detailTextLabel.text = datos.idDato;
    }
    if (indexPath.row == 1) {
        cell.textLabel.text = @"Nombre de la unidad económica";
        cell.detailTextLabel.text = datos.nombreUnidadEconomica;
    }
    if (indexPath.row == 2) {
        cell.textLabel.text = @"Razón Social";
        cell.detailTextLabel.text = datos.razonSocial;
    }
    if (indexPath.row == 3) {
        cell.textLabel.text = @"Código de la clase de actividad";
        cell.detailTextLabel.text = datos.codigoClase;
    }
    if (indexPath.row == 4) {
        cell.textLabel.text = @"Nombre de la clase de actividad";
        cell.detailTextLabel.text = datos.nombreClase;
    }
    if (indexPath.row == 5) {
        cell.textLabel.text = @"Personal Ocupado (estrato)";
        cell.detailTextLabel.text = datos.personalOcupado;
    }
    if (indexPath.row == 6) {
        cell.textLabel.text = @"Tipo de Vialidad";
        cell.detailTextLabel.text = datos.tipoVialidad;
    }
    if (indexPath.row == 7) {
        cell.textLabel.text = @"Nombre de la Vialidad";
        cell.detailTextLabel.text = datos.nombreVialidad;
    }
    if (indexPath.row == 8) {
        cell.textLabel.text = @"Número exterior o km.";
        cell.detailTextLabel.text = datos.numeroExterior;
    }
    if (indexPath.row == 9) {
        cell.textLabel.text = @"Edificio, piso o nivel";
        cell.detailTextLabel.text = datos.edificio;
    }
    if (indexPath.row == 10) {
        cell.textLabel.text = @"Número o letra interior";
        cell.detailTextLabel.text = datos.numeroInterior;
    }
    if (indexPath.row == 11) {
        cell.textLabel.text = @"Tipo y nombre del asentamiento humano";
        cell.detailTextLabel.text = datos.nombreAsentamiento;
    }
    if (indexPath.row == 12) {
        cell.textLabel.text = @"Corredor Industrial, centro comercial o mercado público";
        cell.detailTextLabel.text = datos.corredorIndustrial;
    }
    if (indexPath.row == 13) {
        cell.textLabel.text = @"Número del Local";
        cell.detailTextLabel.text = datos.numeroLocal;
    }
    if (indexPath.row == 14) {
        cell.textLabel.text = @"Código Postal";
        cell.detailTextLabel.text = datos.codigoPostal;
    }
    if (indexPath.row == 15) {
        cell.textLabel.text = @"Entidad Federativa";
        cell.detailTextLabel.text = datos.entidadFederativa;
    }
    if (indexPath.row == 16) {
        cell.textLabel.text = @"Municipio";
        cell.detailTextLabel.text = datos.municipio;
    }
    if (indexPath.row == 17) {
        cell.textLabel.text = @"Localidad";
        cell.detailTextLabel.text = datos.localidad;
    }
    if (indexPath.row == 18) {
        cell.textLabel.text = @"Área geoestadística básica";
        cell.detailTextLabel.text = datos.areaGeoestadistica;
    }
    if (indexPath.row == 19) {
        cell.textLabel.text = @"Manzana";
        cell.detailTextLabel.text = datos.manzana;
    }
    if (indexPath.row == 20) {
        cell.textLabel.text = @"Número de teléfono";
        cell.detailTextLabel.text = datos.telefono;
    }
    if (indexPath.row == 21) {
        cell.textLabel.text = @"Correo electrónico";
        cell.detailTextLabel.text = datos.email;
    }
    if (indexPath.row == 22) {
        cell.textLabel.text = @"Sitio de Internet";
        cell.detailTextLabel.text = datos.website;
    }
    if (indexPath.row == 23) {
        cell.textLabel.text = @"Tipo de Unidad Económica";
        cell.detailTextLabel.text = datos.tipoUnidadEconomica;
    }
    if (indexPath.row == 24) {
        cell.textLabel.text = @"Latitud";
        cell.detailTextLabel.text = datos.latitud;
    }
    if (indexPath.row == 25) {
        cell.textLabel.text = @"Longitud";
        cell.detailTextLabel.text = datos.longitud;
    }
    if (indexPath.row == 26) {
        cell.textLabel.text = @"Fecha de Incorporación al DENUE";
        cell.detailTextLabel.text = datos.fechaIncorporacion;
    }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
