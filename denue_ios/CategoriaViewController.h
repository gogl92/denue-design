//
//  CategoriaViewController.h
//  inegiDENUE
//
//  Created by Jorge Ernesto Mauricio on 11/2/12.
//  Copyright (c) 2012 Jorge Ernesto Mauricio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MapViewController;
@class ResultadosViewController;

@interface CategoriaViewController : UIViewController
{
    AppDelegate *appDelegate;
    MapViewController *mapViewController;
    ResultadosViewController *resultadosView;
}

@property (strong, nonatomic) UITabBarController *tabBarController;

-(IBAction)cafeteria:(id)sender;
-(IBAction)restaurant:(id)sender;
-(IBAction)hospital:(id)sender;
-(IBAction)abarrote:(id)sender;
-(IBAction)parque:(id)sender;
-(IBAction)bar:(id)sender;
-(IBAction)antro:(id)sender;
-(IBAction)banco:(id)sender;
-(IBAction)escuela:(id)sender;
-(IBAction)hotel:(id)sender;
-(IBAction)gasolinera:(id)sender;
-(IBAction)centroComercial:(id)sender;

-(void)changeView;

@end
