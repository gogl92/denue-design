//
//  GetDatos.m
//  DENUE
//
//  Created by Jorge Mauricio on 9/2/12.
//  Copyright (c) 2012 Jorge Mauricio. All rights reserved.
//

#import "GetDatos.h"

@implementation GetDatos

@synthesize xmlData, connectionInProgress, url;
@synthesize delegate;
@synthesize xmlToPost;
@synthesize responseXML;
@synthesize arrayString;
@synthesize titleString;
@synthesize stringCondicion;
@synthesize condicionString;
@synthesize auxEntidad, auxMunicipio;

-(void)startConnection
{
    
    //init AppDelegate
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    //stringCondicion = @"((CONTAINS(DE.BUSQUEDA,''FORMSOF(INFLECTIONAL, pinturas)'')))";
    
    //remove all the objetcs in arrayInfo
    [appDelegate.arrayInfo removeAllObjects];
    
    //Generate the query
    
    NSLog(@"***** %@",appDelegate.tipoBusqueda);
    
    if ([appDelegate.tipoBusqueda isEqualToString:@"1"]) {
        
         condicionString = [[NSMutableString alloc] init];
        NSString *tempString = [NSString stringWithFormat:@"GEOG.STDistance(GEOGRAPHY::Point(%@, %@, 4326)) &lt; %@ AND ",appDelegate.userLatitude, appDelegate.userLongitude, appDelegate.radioBusqueda];
        [condicionString appendString:tempString];
        
        NSArray *arrayTemp = [appDelegate.query componentsSeparatedByString:@" "];
        
        NSLog(@"%@",arrayTemp);
        
        for (int x = 0; x<[arrayTemp count]; x++) {
            [condicionString appendString:[NSString stringWithFormat:@"CONTAINS(DE.BUSQUEDA,''FORMSOF(INFLECTIONAL,%@)'')",[arrayTemp objectAtIndex:x]]];
            
            if ((x+1) == [arrayTemp count]) {
                
            }else{
                [condicionString appendString:@" AND "];
            }
        }
        
    }else if([appDelegate.tipoBusqueda isEqualToString:@"2"])
    {
        condicionString = [[NSMutableString alloc] init];
        NSArray *arrayTemp = [appDelegate.query componentsSeparatedByString:@" "];
        
        NSLog(@"%@",arrayTemp);
        
        for (int x = 0; x<[arrayTemp count]; x++) {
            [condicionString appendString:[NSString stringWithFormat:@"CONTAINS(DE.BUSQUEDA,''FORMSOF(INFLECTIONAL,%@)'')",[arrayTemp objectAtIndex:x]]];
            
            if ((x+1) == [arrayTemp count]) {
                
            }else{
                [condicionString appendString:@" AND "];
            }
        }
    }else if ([appDelegate.tipoBusqueda isEqualToString:@"3"])
    {
        condicionString = [[NSMutableString alloc] init];
        NSArray *arrayTemp = [appDelegate.query componentsSeparatedByString:@" "];
        
        NSLog(@"%@",arrayTemp);
        
        for (int x = 0; x<[arrayTemp count]; x++) {
            [condicionString appendString:[NSString stringWithFormat:@"CONTAINS(DE.BUSQUEDA,''FORMSOF(INFLECTIONAL,%@)'')",[arrayTemp objectAtIndex:x]]];
            
            if ((x+1) == [arrayTemp count]) {
                
            }else{
                [condicionString appendString:@" AND "];
            }
        }

        NSString *tempString = [NSString stringWithFormat:@" AND (DE.entidad_id=''%@'' AND DE.municipio_id=''%@'')",appDelegate.id_entidad, appDelegate.id_localidad];
        
        [condicionString appendString:tempString];
    }else if ([appDelegate.tipoBusqueda isEqualToString:@"4"])
    {
        condicionString = [[NSMutableString alloc] init];
        if ([appDelegate.id_entidad length] == 1) {
            auxEntidad = [NSString stringWithFormat:@"0%@",appDelegate.id_entidad];
        }else if ([appDelegate.id_localidad length] == 1) {
            auxMunicipio = [NSString stringWithFormat:@"00%@",appDelegate.id_localidad];
        }
        NSString *tempString = [NSString stringWithFormat:@"(DE.entidad_id=''%@'' AND DE.municipio_id=''%@'')",auxEntidad, auxMunicipio];
        
        [condicionString appendString:tempString];
    }else if([appDelegate.tipoBusqueda isEqualToString:@"5"])
    {
        condicionString = [[NSMutableString alloc] init];
        
        //Validación CLASE_ACTIVIDAD_ID
        if ([appDelegate.id_categoria isEqualToString:@"461110"] || [appDelegate.id_categoria isEqualToString:@"722411"] || [appDelegate.id_categoria isEqualToString:@"522110"] || [appDelegate.id_categoria isEqualToString:@"722412"] ||
            [appDelegate.id_categoria isEqualToString:@"722219"] ||[appDelegate.id_categoria isEqualToString:@"531311"]) {
            [condicionString appendString:[NSString stringWithFormat:@"GEOG.STDistance(GEOGRAPHY::Point(%@, %@, 4326)) &lt; %@ AND (DE.clase_actividad_id=''%@'')",appDelegate.userLatitude, appDelegate.userLongitude, appDelegate.radioBusqueda, appDelegate.id_categoria]];
        }
        
        //VALIDACION SECTOR_ACTIVIDAD_ID
        if ([appDelegate.id_categoria isEqualToString:@"61"] || [appDelegate.id_categoria isEqualToString:@"468411"]) {
            [condicionString appendString:[NSString stringWithFormat:@"GEOG.STDistance(GEOGRAPHY::Point(%@, %@, 4326)) &lt; %@ AND (DE.sector_actividad_id=''%@'')",appDelegate.userLatitude, appDelegate.userLongitude,appDelegate.radioBusqueda, appDelegate.id_categoria]];
        }
        
        //VALIDAION SUBSECTOR_ACTIVIDAD_ID
        if ([appDelegate.id_categoria isEqualToString:@"622"]) {
            [condicionString appendString:[NSString stringWithFormat:@"GEOG.STDistance(GEOGRAPHY::Point(%@, %@, 4326)) &lt; %@ AND (DE.subsector_actividad_id=''%@'')",appDelegate.userLatitude, appDelegate.userLongitude,appDelegate.radioBusqueda, appDelegate.id_categoria]];
        }
        
        //VALIDACION SUBRAMA_ACTIVIDAD_ID
        if ([appDelegate.id_categoria isEqualToString:@"622"]) {
            [condicionString appendString:[NSString stringWithFormat:@"GEOG.STDistance(GEOGRAPHY::Point(%@, %@, 4326)) &lt; %@ AND (DE.subrama_actividad_id=''%@'')",appDelegate.userLatitude, appDelegate.userLongitude,appDelegate.radioBusqueda, appDelegate.id_categoria]];
        }
        
        //VALIDACION RAMA_ACTIVIDAD_ID
        if ([appDelegate.id_categoria isEqualToString:@"7221"]) {
            [condicionString appendString:[NSString stringWithFormat:@"GEOG.STDistance(GEOGRAPHY::Point(%@, %@, 4326)) &lt; %@ AND (DE.rama_actividad_id=''%@'')",appDelegate.userLatitude, appDelegate.userLongitude,appDelegate.radioBusqueda, appDelegate.id_categoria]];
        }
    
    }
    
    // SOAP 1.1
    
    xmlToPost = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                 "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
                 "<soap:Body>\n"
                 "<consultaSP xmlns=\"http://tempuri.org/\">\n"
                 "<condicion>%@</condicion>\n"
                 "<inic>0</inic>\n"
                 "<topeT>%@</topeT>\n"
                 "</consultaSP>\n"
                 "</soap:Body>\n"
                 "</soap:Envelope>",condicionString, appDelegate.topeQ];
    
    // SOAP 1.2
    
    xmlToPost = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                     "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">\n"
                     "<soap12:Body>\n"
                     "<consultaSP xmlns=\"http://tempuri.org/\">\n"
                     "<condicion>%@</condicion>\n"
                     "<inic>0</inic>\n"
                     "<topeT>%@</topeT>\n"
                     "</consultaSP>\n"
                     "</soap12:Body>\n"
                     "</soap12:Envelope>",condicionString,appDelegate.topeQ];
        
        [xmlToPost writeToFile:[self getPath] atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    // POST CODE

    // SOAP 1.1
    
    
    url = [NSURL URLWithString:@"http://www5.inegi.org.mx/sistemas/mapa/Denue/webservice1.asmx?op=consultaSP"];
     NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
     
     //---set the headers---
     NSString *msgLength = [NSString stringWithFormat:@"%d",[xmlToPost length]];
    [req setHTTPMethod:@"POST"];
    [req setHTTPBody: [NSData dataWithContentsOfFile:[self getPath]]];
    [req addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [req addValue:@"http://tempuri.org/consultaSP" forHTTPHeaderField:@"SOAPAction"];
    [req addValue:msgLength forHTTPHeaderField:@"Content-Length"];
    
    NSLog(@"%@",msgLength);
    
    
    //GET
    
    /*
    url = [NSURL URLWithString:@"http://www5.inegi.org.mx/sistemas/mapa/Denue/webservice1.asmx?op=consultaSP?"];
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
    
    //---set the headers---
    NSString *msgLength = [NSString stringWithFormat:@"%d",[xmlToPost length]];
    [req setHTTPMethod:@"POST"];
    [req setHTTPBody: [NSData dataWithContentsOfFile:[self getPath]]];
    [req addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [req addValue:@"http://tempuri.org/consultaSP" forHTTPHeaderField:@"SOAPAction"];
    [req addValue:msgLength forHTTPHeaderField:@"Content-Length"];
    
    NSLog(@"%@",msgLength);
    
    */
    //SOAP 1.2
    
    /*
    url = [NSURL URLWithString:@"http://www5.inegi.org.mx/sistemas/mapa/Denue/webservice1.asmx/consultaSP"];
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
    
    //---set the headers---
    NSString *msgLength = [NSString stringWithFormat:@"%d",[xmlToPost length]];
    [req setHTTPMethod:@"POST"];
    [req setHTTPBody: [NSData dataWithContentsOfFile:[self getPath]]];
    [req addValue:@"application/soap+xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [req addValue:msgLength forHTTPHeaderField:@"Content-Length"];
    */
     
    connectionInProgress = [[NSURLConnection alloc] initWithRequest:req delegate:self];
     if (connectionInProgress)
     {
         xmlData = [[NSMutableData alloc] init];
     }
     else
     {
         NSLog(@"theConnection is NULL");
     }
    
    NSLog(@"%@",xmlToPost);
    
    /* * * * * * * * * *
    //init arrrayQuestionsAsesor
    [appDelegate.arrayInfo removeAllObjects];
    
    //Print Start
    NSLog(@"StartProcess");
    
    //init URL Local
    //url = [NSURL URLWithString:@"http://10.32.3.105:8080/ahsa/getCuestionario"];
    
    //init URL Server
   url = [NSURL URLWithString:@"https://dl.dropbox.com/u/2182160/denue.json"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    if (connectionInProgress)
    {
        NSLog(@"Connection in Progress");
        [connectionInProgress cancel];
    }
    xmlData = [[NSMutableData alloc] init];
    connectionInProgress = [[NSURLConnection alloc] initWithRequest:request delegate:self];
     **/
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [xmlData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    responseXML = [[NSString alloc] initWithData:xmlData encoding:NSUTF8StringEncoding];
    
    NSLog(@"\n * * * * * * * \n%@", responseXML);
    
    
    //init arrrays
    arrayString = [[NSMutableArray alloc] init];

    //Make the information array
    
    parser = [[NSXMLParser alloc] initWithData:xmlData];
    parser.delegate = self;
    [parser parse];

    NSLog(@"%i",[arrayString count]);
    
    int ciclos= ([arrayString count]/27);
    
    NSLog(@"%i",ciclos);

   
    
    for (int x = 0; x<ciclos; x++) {
        //init PreguntasAsesor
        
        datos = [[Datos alloc] init];
        
        datos.idDato = [arrayString objectAtIndex:(0+(27*x))];
        datos.nombreUnidadEconomica = [arrayString objectAtIndex:(8+(27*x))];
        datos.razonSocial = [arrayString objectAtIndex:(9+(27*x))];
        datos.codigoClase = [arrayString objectAtIndex:(6+(27*x))];
        datos.nombreClase = [arrayString objectAtIndex:(7+(27*x))];
        datos.personalOcupado = [arrayString objectAtIndex:(23+(27*x))];
        datos.tipoVialidad = [arrayString objectAtIndex:(10+(27*x))];
        datos.nombreVialidad = [arrayString objectAtIndex:(11+(27*x))];
        datos.numeroExterior = [arrayString objectAtIndex:(15+(27*x))];
        datos.edificio = [arrayString objectAtIndex:(14+(27*x))];
        datos.numeroInterior = [arrayString objectAtIndex:(16+(27*x))];
        datos.nombreAsentamiento = [arrayString objectAtIndex:(12+(27*x))];
        datos.corredorIndustrial = [arrayString objectAtIndex:(13+(27*x))];
        datos.numeroLocal = [arrayString objectAtIndex:(17+(27*x))];
        datos.codigoPostal = [arrayString objectAtIndex:(18+(27*x))];
        datos.entidadFederativa = [arrayString objectAtIndex:(1+(27*x))];
        datos.municipio = [arrayString objectAtIndex:(2+(27*x))];
        datos.localidad = [arrayString objectAtIndex:(3+(27*x))];
        datos.areaGeoestadistica = [arrayString objectAtIndex:(4+(27*x))];
        datos.manzana = [arrayString objectAtIndex:(5+(27*x))];
        datos.telefono = [arrayString objectAtIndex:(19+(27*x))];
        datos.email = [arrayString objectAtIndex:(20+(27*x))];
        datos.website = [arrayString objectAtIndex:(21+(27*x))];
        datos.tipoUnidadEconomica = [arrayString objectAtIndex:(22+(27*x))];
        datos.latitud = [arrayString objectAtIndex:(24+(27*x))];
        datos.longitud = [arrayString objectAtIndex:(25+(27*x))];
        datos.fechaIncorporacion = [arrayString objectAtIndex:(26+(27*x))];
        
        [appDelegate.arrayInfo addObject:datos];
        
        NSLog(@"* * * * *  %i",[appDelegate.arrayInfo count]);

    }
    /*
    for (int x = 0; x<[informacionArray count]; x++)
    {
        //init PreguntasAsesor
        Datos *datos = [[Datos alloc] init];
        
        //init Dato Dictionary
        NSDictionary *dato = [informacionArray objectAtIndex:x];
        
        datos.idDato = [dato objectForKey:@"idDato"];
        datos.nombreUnidadEconomica = [dato objectForKey:@"nombreUnidadEconomica"];
        datos.razonSocial = [dato objectForKey:@"razonSocial"];
        datos.codigoClase = [dato objectForKey:@"codigoClase"];
        datos.nombreClase = [dato objectForKey:@"nombreClase"];
        datos.personalOcupado = [dato objectForKey:@"personalOcupado"];
        datos.tipoVialidad = [dato objectForKey:@"tipoVialidad"];
        datos.nombreVialidad = [dato objectForKey:@"nombreVialidad"];
        datos.numeroExterior = [dato objectForKey:@"numeroExterior"];
        datos.edificio = [dato objectForKey:@"edificio"];
        datos.numeroInterior = [dato objectForKey:@"numeroInterior"];
        datos.nombreAsentamiento = [dato objectForKey:@"nombreAsentamiento"];
        datos.corredorIndustrial = [dato objectForKey:@"corredorIndustrial"];
        datos.numeroLocal = [dato objectForKey:@"numeroLocal"];
        datos.codigoPostal = [dato objectForKey:@"codigoPostal"];
        datos.entidadFederativa = [dato objectForKey:@"entidadFederativa"];
        datos.municipio = [dato objectForKey:@"municipio"];
        datos.localidad = [dato objectForKey:@"localidad"];
        datos.areaGeoestadistica = [dato objectForKey:@"areaGeoestadistica"];
        datos.manzana = [dato objectForKey:@"manzana"];
        datos.telefono = [dato objectForKey:@"telefono"];
        datos.email = [dato objectForKey:@"email"];
        datos.website = [dato objectForKey:@"website"];
        datos.tipoUnidadEconomica = [dato objectForKey:@"tipoUnidadEconomica"];
        datos.latitud = [dato objectForKey:@"latitud"];
        datos.longitud = [dato objectForKey:@"longitud"];
        datos.fechaIncorporacion = [dato objectForKey:@"fechaIncorporacion"];
        [appDelegate.arrayInfo addObject:datos];
    }
    */
    NSLog(@"Parse Done");
    [self.delegate returnResponse];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Error");
}

-(void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
 namespaceURI:(NSString *)namespaceURI
qualifiedName:(NSString *)qName
   attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqual:@"ArrayOfString"]) {
        NSLog(@"Found a title ArrayOfString");
        waitingForEntryTitle = YES;
    }
    if ([elementName isEqual:@"string"] && waitingForEntryTitle) {
        NSLog(@"found string");
        titleString = [[NSMutableString alloc] init];
    }
    if ([elementName isEqual:@"string /"] && waitingForEntryTitle) {
        NSLog(@"found string /");
        titleString = [[NSMutableString alloc] init];
    }
}

-(void)parser:(NSXMLParser *)parser
didEndElement:(NSString *)elementName
 namespaceURI:(NSString *)namespaceURI
qualifiedName:(NSString *)qName
{

    if ([elementName isEqual:@"string /"] && waitingForEntryTitle) {
        NSLog(@"end string /");
        //[arrayString addObject:titleString];
    }
    if ([elementName isEqual:@"string"] && waitingForEntryTitle) {
        NSLog(@"end string");
        [arrayString addObject:titleString];
    }
    if ([elementName isEqual:@"ArrayOfString"]) {
        NSLog(@"end a title ArrayOfString");
        waitingForEntryTitle = NO;
    }
    
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    NSString *trimmedString = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [titleString appendString:trimmedString];
}


-(NSString*)getPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"info.xml"];
    
    return path;
    
}

@end
