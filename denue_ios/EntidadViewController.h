//
//  EntidadViewController.h
//  inegiDENUE
//
//  Created by Jorge Ernesto Mauricio on 11/20/12.
//  Copyright (c) 2012 Jorge Ernesto Mauricio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Entidad.h"
#import "MunicipioViewController.h"

@interface EntidadViewController : UITableViewController
{
    AppDelegate *appDelegate;
    MunicipioViewController *municipioView;
}

@property (strong, nonatomic) NSMutableArray *arrayEntidades;
@property (strong, nonatomic) NSString *responseJSON;
@property (strong, nonatomic) NSURLConnection *connectionInProgress;
@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) NSMutableData *jsonData;

-(NSString*)getPath;
    

@end
