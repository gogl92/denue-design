//
//  ViewController.h
//  inegiDENUE
//
//  Created by Jorge Ernesto Mauricio on 10/27/12.
//  Copyright (c) 2012 Jorge Ernesto Mauricio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ConfiguracionViewController.h"

@class MapViewController, ResultadosViewController, CategoriaViewController, ConfiguracionViewController;

@interface ViewController : UIViewController<UITextFieldDelegate,CLLocationManagerDelegate>
{
    MapViewController *mapViewController;
    AppDelegate *appDelegate;
    ResultadosViewController *resultadosView;
    CategoriaViewController *categoriaView;
    ConfiguracionViewController *configuracionView;
}

@property (strong, nonatomic) IBOutlet UITextField *queryText;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (strong, nonatomic) UITabBarController *tabBarController;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) IBOutlet UIButton *buttonInfo;

-(void)searchQuery;

-(BOOL)textFieldShouldReturn:(UITextField *)textField;

-(IBAction)goInfo:(id)sender;



@end
