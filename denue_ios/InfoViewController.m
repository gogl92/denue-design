//
//  InfoViewController.m
//  inegiDENUE
//
//  Created by Jorge Ernesto Mauricio on 11/4/12.
//  Copyright (c) 2012 Jorge Ernesto Mauricio. All rights reserved.
//

#import "InfoViewController.h"

@interface InfoViewController ()

@end

@implementation InfoViewController
@synthesize arrayNumbers;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Número de resultados";
    }
    return self;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [arrayNumbers count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [arrayNumbers objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    appDelegate.topeQ = [arrayNumbers objectAtIndex:row];
    
    NSLog(@"Selected Number: %@.", [arrayNumbers objectAtIndex:row]);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //appDelegate
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    //init arrayNumbers
    arrayNumbers = [[NSMutableArray alloc] initWithObjects:@"5",
                    @"10",
                    @"20",
                    @"30",
                    @"40",
                    @"50",
                    @"60",
                    @"70",
                    @"80",
                    @"90",
                    @"100",nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
