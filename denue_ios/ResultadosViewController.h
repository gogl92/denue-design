//
//  ResultadosViewController.h
//  DENUE
//
//  Created by Jorge Mauricio on 9/2/12.
//  Copyright (c) 2012 Jorge Mauricio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GetDatos.h"
#import "MapViewController.h"

@class DetailViewController;

@interface ResultadosViewController : UITableViewController <GetDatosDelegate>
{
    AppDelegate *appDelegate;
    DetailViewController *detailViewController;
    MapViewController *mapViewController;
}

@end
