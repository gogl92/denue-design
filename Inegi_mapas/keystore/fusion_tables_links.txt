Getting Started
https://developers.google.com/fusiontables/docs/v1/getting_started

Using the API
https://developers.google.com/fusiontables/docs/v1/using#auth

Samples and Libraries
https://developers.google.com/fusiontables/docs/v1/libraries

Google API Explorer
https://developers.google.com/apis-explorer/#p/fusiontables/v1/

Google API client library for Java
http://code.google.com/p/google-api-java-client/

Service Account for OAuth2:
http://code.google.com/p/google-api-java-client/wiki/OAuth2

FusionTables API
http://code.google.com/p/google-api-java-client/wiki/APIs#Fusion_Tables_API

-------
Service Accounts Settings:
private key password: notasecret.

clientID: 736466375713.apps.googleusercontent.com
email Address: 736466375713@developer.gserviceaccount.com
public key fingerprint: 3edeee067ef79ebfed4fc58e337669b0d8edb1c2

p12 original name: 3edeee067ef79ebfed4fc58e337669b0d8edb1c2-privatekey.p12