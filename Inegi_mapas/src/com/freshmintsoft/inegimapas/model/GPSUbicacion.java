package com.freshmintsoft.inegimapas.model;

import com.google.android.maps.GeoPoint;

/**
 * Clase de ayuda que guarda referencia
 * a la longitud y latitud.
 * @author freshmintsoft
 *
 */
public class GPSUbicacion {

	private Double latitud;
	private Double longitud;
	private Double altitud;
	
	public GPSUbicacion(){
		super();
	}
	
	public GPSUbicacion(String longitud, String latitud, String altitud){
		this.latitud = Double.parseDouble(latitud);
		this.longitud = Double.parseDouble(longitud);
		this.altitud = Double.parseDouble(altitud);
	}
	
	public GPSUbicacion(String longitud, String latitud){
		this.latitud = Double.parseDouble(latitud);
		this.longitud = Double.parseDouble(longitud);
	}
	
	public Double getLatitud() {
		return latitud;
	}
	
	public void setLatitud(Double latitud) {
		this.latitud = latitud;
	}
	
	public Double getLongitud() {
		return longitud;
	}
	
	public void setLongitud(Double longitud) {
		this.longitud = longitud;
	}
	
	/**
	 * Convierte la ubicacion a su representativo GeoPoint
	 * que es el formato que Google Maps reconoce y pinta.
	 * @return
	 */
	public GeoPoint toGeoPoint(){
		return new GeoPoint( (int) (latitud * 1e6), (int) (longitud * 1e6)); 
	}
}
