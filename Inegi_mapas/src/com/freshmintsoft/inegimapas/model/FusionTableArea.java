package com.freshmintsoft.inegimapas.model;

import java.util.List;

public class FusionTableArea {

	private String areaName;
	private List<GPSUbicacion> coordinates;
	
	public FusionTableArea(String areaName){
		super();
		this.areaName = areaName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public List<GPSUbicacion> getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(List<GPSUbicacion> coordinates) {
		this.coordinates = coordinates;
	}
	
}
