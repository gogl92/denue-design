package com.freshmintsoft.inegimapas;



import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.ArrayAdapter;

public class MunicipiosDialogFragment extends DialogFragment{

	ArrayAdapter<String> aa;
	private String[] contentArray;
	DenueApplication application;
	
	public interface MunicipiosListener{
		public void onMunicipioSelected();
	}
	
	MunicipiosListener listener;
	static MunicipiosDialogFragment newInstace(){
		MunicipiosDialogFragment d = new MunicipiosDialogFragment();
		Bundle args = new Bundle();
		d.setArguments(args);
		
		return d;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		application = (DenueApplication) getActivity().getApplication();
		listener = (MunicipiosListener) getActivity();
	}

	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		super.onCreateDialog(savedInstanceState);
		getMunicipioFromLocalidad();
		aa = new ArrayAdapter<String>(getActivity(), 
											android.R.layout.simple_list_item_1, 
											contentArray);
		return new AlertDialog.Builder(getActivity())
		.setIcon(R.drawable.device_map_overlay)
		.setTitle("Selecciona el Municipio")
		.setAdapter(aa, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				application.municipio = which;
				listener.onMunicipioSelected();
			}
		})
		.create();
	}
	
	private void getMunicipioFromLocalidad(){
		switch(application.localidad){
			case 1:
				contentArray = getResources().getStringArray(R.array.municipios_01);
				break;
			case 2:
				contentArray = getResources().getStringArray(R.array.municipios_02);
				break;
			case 3:
				contentArray = getResources().getStringArray(R.array.municipios_03);
				break;
			case 4:
				contentArray = getResources().getStringArray(R.array.municipios_04);
				break;
			case 5:
				contentArray = getResources().getStringArray(R.array.municipios_05);
				break;
			case 6:
				contentArray = getResources().getStringArray(R.array.municipios_06);
				break;
			case 7:
				contentArray = getResources().getStringArray(R.array.municipios_07);
				break;
			case 8:
				contentArray = getResources().getStringArray(R.array.municipios_08);
				break;	
			case 9:
				contentArray = getResources().getStringArray(R.array.municipios_09);
				break;
			case 10:
				contentArray = getResources().getStringArray(R.array.municipios_10);
				break;
			case 11:
				contentArray = getResources().getStringArray(R.array.municipios_11);
				break;
			case 12:
				contentArray = getResources().getStringArray(R.array.municipios_12);
				break;
			case 13:
				contentArray = getResources().getStringArray(R.array.municipios_13);
				break;
			case 14:
				contentArray = getResources().getStringArray(R.array.municipios_14);
				break;
			case 15:
				contentArray = getResources().getStringArray(R.array.municipios_15);
				break;
			case 16:
				contentArray = getResources().getStringArray(R.array.municipios_16);
				break;
			case 17:
				contentArray = getResources().getStringArray(R.array.municipios_17);
				break;
			case 18:
				contentArray = getResources().getStringArray(R.array.municipios_18);
				break;
			case 19:
				contentArray = getResources().getStringArray(R.array.municipios_19);
				break;
			case 20:
				contentArray = getResources().getStringArray(R.array.municipios_20);
				break;
			case 21:
				contentArray = getResources().getStringArray(R.array.municipios_21);
				break;
			case 22:
				contentArray = getResources().getStringArray(R.array.municipios_22);
				break;
			case 23:
				contentArray = getResources().getStringArray(R.array.municipios_23);
				break;
			case 24:
				contentArray = getResources().getStringArray(R.array.municipios_24);
				break;
			case 25:
				contentArray = getResources().getStringArray(R.array.municipios_25);
				break;
			case 26:
				contentArray = getResources().getStringArray(R.array.municipios_26);
				break;
			case 27:
				contentArray = getResources().getStringArray(R.array.municipios_27);
				break;
			case 28:
				contentArray = getResources().getStringArray(R.array.municipios_28);
				break;
			case 29:
				contentArray = getResources().getStringArray(R.array.municipios_29);
				break;
			case 30:
				contentArray = getResources().getStringArray(R.array.municipios_30);
				break;
			case 31:
				contentArray = getResources().getStringArray(R.array.municipios_31);
				break;
			case 32:
				contentArray = getResources().getStringArray(R.array.municipios_32);
				break;
		}
		application.municipiosArray = contentArray;
	}
	
}
