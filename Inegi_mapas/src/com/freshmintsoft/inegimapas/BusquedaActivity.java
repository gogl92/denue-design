package com.freshmintsoft.inegimapas;

import android.content.Context;
import java.util.StringTokenizer;

import com.freshmintsoft.inegimapas.model.GPSUbicacion;
import com.freshmintsoft.inegimapas.service.DenueQueryServiceInterface;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class BusquedaActivity extends DenueActivity implements OnClickListener,
        RespuestaInegiListener,
        LocalidadesDialogFragment.LocalidadesListener,
        MunicipiosDialogFragment.MunicipiosListener {

    public static String BUSQUEDA_INPUT_TEXT = "BUSQUEDA_INPUT_TEXT";
    EditText busquedaTextView;
    TextView hiddenText;
    TextView rangoText;
    Button enviar;
    String text;
    String seleccionEstadoText;
    String buttonEstadoText;
    DenueApplication application;
    ToggleButton toggleButton;
    Button estadosButton;
    String busquedaText;
    ImageButton hoteles;
    ImageButton hospitales;
    ImageButton restaurantes;
    ImageButton bares;
    ImageButton antros;
    ImageButton parques;
    ImageButton centrosComerciales;
    ImageButton cafeterias;
    ImageButton abarrotes;
    ImageButton bancos;
    ImageButton escuelas;
    ImageButton gasolineras;
    private static final String QUERY_BASE = "CONTAINS(DE.BUSQUEDA,''FORMSOF(INFLECTIONAL,";
    private static final String QUERY_BASE_END = ")'')";
    private static final String QUERY_GPS_BASE = "GEOG.STDistance(GEOGRAPHY::Point(";
    private static final String QUERY_GPS_BASE_1000_END = ", 4326)) &lt; 1000";
    private static final String QUERY_GPS_BASE_500_END = ", 4326)) &lt; 500";
    private static final String QUERY_GPS_BASE_100_END = ", 4326)) &lt; 100";
    private static final String QUERY_GPS_BASE_5000_END = ", 4326)) &lt; 5000";
    private static final String QUERY_ENTIDAD = "(DE.entidad_id=''";
    private static final String QUERY_ENTIDAD_END = "'')";
    private static final String QUERY_MUNICIPIO = "DE.municipio_id=''";
    private static final String QUERY_MUNICIPIO_END = "'')";
    private static final String TAG = "[Busqueda Activity]";
    private Thread thread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busqueda);
        application = (DenueApplication) getApplication();
        application.detalleIndexSelected = -1;

        initializeComponents();
        setEnviarButtonHandler();
        setToggleHandler();
        setEstadosHandler();
        setButtonsHandler();
        addEnterKeySubmit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        setRangoBusqueda();
        super.unlockScreenOrientation();
    }

    private void initializeComponents() {
        rangoText = (TextView) findViewById(R.id.busqueda_rango_busqueda);
        toggleButton = (ToggleButton) findViewById(R.id.busqueda_toggle_switch);
        estadosButton = (Button) findViewById(R.id.busqueda_selecciona_estado);
        busquedaTextView = (EditText) findViewById(R.id.busqueda_input);
        enviar = (Button) findViewById(R.id.busqueda_enviar);
        hiddenText = (TextView) findViewById(R.id.busqueda_error);
        hoteles = (ImageButton) findViewById(R.id.categoria_hoteles);
        hospitales = (ImageButton) findViewById(R.id.categoria_hospitales);
        restaurantes = (ImageButton) findViewById(R.id.categoria_restaurantes);
        bares = (ImageButton) findViewById(R.id.categoria_bares);
        antros = (ImageButton) findViewById(R.id.categoria_antros);
        parques = (ImageButton) findViewById(R.id.categoria_parques);
        centrosComerciales = (ImageButton) findViewById(R.id.categoria_centrosComerciales);
        cafeterias = (ImageButton) findViewById(R.id.categoria_cafeteria);
        abarrotes = (ImageButton) findViewById(R.id.categoria_abarrotes);
        bancos = (ImageButton) findViewById(R.id.categoria_bancos);
        escuelas = (ImageButton) findViewById(R.id.categoria_escuelas);
        gasolineras = (ImageButton) findViewById(R.id.categoria_gasolineras);
        
        getWindow().setSoftInputMode(
        	    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void setButtonsHandler() {
        hoteles.setOnClickListener(this);
        hospitales.setOnClickListener(this);
        restaurantes.setOnClickListener(this);
        bares.setOnClickListener(this);
        antros.setOnClickListener(this);
        parques.setOnClickListener(this);
        centrosComerciales.setOnClickListener(this);
        cafeterias.setOnClickListener(this);
        abarrotes.setOnClickListener(this);
        bancos.setOnClickListener(this);
        escuelas.setOnClickListener(this);
        gasolineras.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        setBusquedaBasedOnButtonClicked(v);
        sendDataToService();
    }

    private void setBusquedaBasedOnButtonClicked(View v) {
        switch (v.getId()) {
            case R.id.categoria_abarrotes:
                text = "((DE.CLASE_ACTIVIDAD_ID=''461110''))";
                break;
            case R.id.categoria_antros:
                text = "((DE.CLASE_ACTIVIDAD_ID=''722411''))";
                break;
            case R.id.categoria_bancos:
                text = "((DE.CLASE_ACTIVIDAD_ID=''522110''))";
                break;
            case R.id.categoria_bares:
                text = "((DE.CLASE_ACTIVIDAD_ID=''722412''))";
                break;
            case R.id.categoria_cafeteria:
                text = "((DE.CLASE_ACTIVIDAD_ID=''722219'') OR (DE.CLASE_ACTIVIDAD_ID=''722111'') OR (DE:CLASE_ACTIVIDAD_ID=''722110''))";
                break;
            case R.id.categoria_centrosComerciales:
                text = "((DE.CLASE_ACTIVIDAD_ID=''531311'') OR (DE.CLASE_ACTIVIDAD_ID=''531114''))";
                break;
            case R.id.categoria_escuelas:
                text = "((DE.SECTOR_ACTIVIDAD_ID=''61''))";
                break;
            case R.id.categoria_gasolineras:
                text = "((DE.CLASE_ACTIVIDAD_ID=''468411''))";
                break;
            case R.id.categoria_hospitales:
                text = "(DE.SUBSECTOR_ACTIVIDAD_ID=''622'')";
                break;
            case R.id.categoria_hoteles:
                text = "((DE.SUBRAMA_ACTIVIDAD_ID=''72111'') OR (DE.rama_actividad_id=''72112''))";
                break;
            case R.id.categoria_parques:
                text = "((DE.SUBRAMA_ACTIVIDAD_ID=''71311''))";
                break;
            case R.id.categoria_restaurantes:
                text = "((DE.rama_actividad_id=''7221'') OR (DE.rama_actividad_id=''7222''))";
                break;
        }
    }

    private void addEnterKeySubmit() {
        //Al presionarse la Tecla de enter llama a los metodos para hacer la busqueda.
        busquedaTextView.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            cleanView();
                            if (checkIfTextisEmpty()) {
                                return false;
                            }
                            getInputText();
                            sendDataToService();
                            return true;
                        default:
                            break;
                    }
                }
                return false;

            }
        });
    }

    private void setRangoBusqueda() {
        String text = getResources().getString(R.string.rango_disponible);
        String formatValue = String.format(text, application.rangoDatos);
        CharSequence styledText = Html.fromHtml(formatValue);
        rangoText.setText(styledText);
    }

    private void setEnviarButtonHandler() {
        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cleanView();
                if (checkIfTextisEmpty()) {
                    return;
                }
                getInputText();
                sendDataToService();
            }
        });
    }

    private void setEstadosHandler() {
        estadosButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                application.localidad = 0;
                application.municipio = 0;
                createEstadosDialog();
            }
        });
    }

    private void setToggleHandler() {
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                text = "";
                if (isChecked) {
                    estadosButton.setVisibility(View.INVISIBLE);
                } else {
                    createEstadosDialog();
                    estadosButton.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    private void cleanView() {
        setErrorTextVisibilityToInvisible();
    }

    private void sendDataToService() {
        boolean isToggleSelected = this.getToggleState();
        if (!isToggleSelected) {
            text = text + seleccionEstadoText;
            Log.d(TAG, "ESTADO QUERY>" + text);
            connectToService();
            showProgressDialog();
        } else { 
        	String gpsQuery = createGpsQuery();
        	
            String textTmp = text;
            text = gpsQuery + " AND " + textTmp;
            Log.d("BusquedaActivity: ", "GPS QUERY: " + text);
            connectToService();
            showProgressDialog();
        }

    }

    private String createGpsQuery(){
    	String gpsQuery  = "";
    	GPSUbicacion ubicacion = getLocation();
    	if (ubicacion != null && ubicacion.getLatitud() != null && ubicacion.getLongitud() != null) {
    	
	    	gpsQuery = QUERY_GPS_BASE
	                + ubicacion.getLatitud() + ", "
	                + ubicacion.getLongitud();
	    	int gpsRango = application.gpsRango;
	    	switch(gpsRango){
	    	case 100:
	    		gpsQuery += QUERY_GPS_BASE_100_END;
	    		break;
	    	case 500:
	    		gpsQuery += QUERY_GPS_BASE_500_END;
	    		break;
	    	case 1000:
	    		gpsQuery += QUERY_GPS_BASE_1000_END;
	    		break;
	    	case 5000:
	    		gpsQuery += QUERY_GPS_BASE_5000_END;
	    		break;
	    	default:
	    		gpsQuery += QUERY_GPS_BASE_500_END;
	    	}
    	} else {
    		displayErrorMessage("No se puede obtener tu ubicacion."
                    + "Ve a Menu > Preferencias y selecciona Configuraciones de red para habilitar el GPS.");
    	}
    	return gpsQuery;
    }
    
    private boolean getToggleState() {
        boolean isChecked = toggleButton.isChecked();
        return isChecked;
    }

    private void setErrorTextVisibilityToInvisible() {
        hiddenText.setVisibility(View.INVISIBLE);
    }

    private void getInputText() {
        if (busquedaText.contains(" ")) {
            StringTokenizer tokens = new StringTokenizer(busquedaText, " ");
            text = QUERY_BASE + tokens.nextToken() + QUERY_BASE_END;
            while (tokens.hasMoreTokens()) {
                String token = tokens.nextToken();
                text += " AND " + QUERY_BASE + token + QUERY_BASE_END;
            }
        } else {
            text = QUERY_BASE + busquedaText + QUERY_BASE_END;
        }
        Log.d(TAG, "Multi line query Query >>>" + text);
    }

    private boolean checkIfTextisEmpty() {
        boolean isTextEmpty = false;
        busquedaText = busquedaTextView.getText().toString();
        if (busquedaText == null || busquedaText == "") {
            isTextEmpty = true;
        }
        return isTextEmpty;
    }

    private GPSUbicacion getLocation() {
        GPSUbicacion ubicacion = application.getUbicacion();
        return ubicacion;
    }

    private void createEstadosDialog() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        Fragment prev = manager.findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        LocalidadesDialogFragment localidadesFragment = LocalidadesDialogFragment.newInstace();
        localidadesFragment.show(ft, "dialog");
        super.lockOrientation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        super.lockOrientation();
        application.removeLocationServiceUpdates();

    }

    public void connectToService() {
        DenueQueryServiceInterface service =
                ((DenueApplication) getApplication()).getRespuestaInegiService();
        service.setCallView(this);
        service.queryServer(text, null);
    }

    public void onEstadoSelected() {
        getEstadoSeleccionado();
        estadosButton.setText(buttonEstadoText);
        if (application.localidad == 0) {
            seleccionEstadoText = " AND " + QUERY_ENTIDAD
                    + (application.localidad + 1) + QUERY_ENTIDAD_END;
        } else {
            if (application.localidad < 10) {
                seleccionEstadoText = " AND " + QUERY_ENTIDAD + "0" + application.localidad;
            } else {
                seleccionEstadoText = " AND " + QUERY_ENTIDAD + application.localidad;
            }
            showMunicipiosDialog();
        }
    }

    public void onMunicipioSelected() {
        getMunicipioSeleccionado();
        estadosButton.setText(buttonEstadoText);
        if (application.municipio == 0) {
            seleccionEstadoText += QUERY_ENTIDAD_END;
        } else {
            if (application.municipio < 10) {
                seleccionEstadoText += "'' AND " + QUERY_MUNICIPIO + "00" + application.municipio + QUERY_MUNICIPIO_END;
            } else if (application.municipio < 100) {
                seleccionEstadoText += "'' AND " + QUERY_MUNICIPIO + "0" + application.municipio + QUERY_MUNICIPIO_END;
            } else {
                seleccionEstadoText += "'' AND " + QUERY_MUNICIPIO + application.municipio + QUERY_MUNICIPIO_END;
            }
        }
    }

    private void showMunicipiosDialog() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        Fragment prev = manager.findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        MunicipiosDialogFragment municipiosFragment = MunicipiosDialogFragment.newInstace();
        municipiosFragment.show(ft, "dialog");
        super.lockOrientation();
    }

    private void getEstadoSeleccionado() {
        String[] estados = getResources().getStringArray(R.array.localidades_array);
        buttonEstadoText = estados[application.localidad];
    }

    private void getMunicipioSeleccionado() {
        buttonEstadoText += ":" + application.municipiosArray[application.municipio];
    }

    public void onRespuestaReceived() {
        progressDialog.dismiss();
        if (application.getDenueResultados() != null && application.getDenueResultados().size() != 0) {
            startResultadosActivity();
        } else {
            displayErrorMessage("Sin resultados, Intenta de nuevo.");
        }
    }

    private void startResultadosActivity() {
        Intent i = new Intent(getBaseContext(), TabMapsListActivity.class);
        startActivity(i);
    }

    private void displayErrorMessage(String text) {
//        hiddenText.setVisibility(View.VISIBLE);
//        hiddenText.setText(text);
//        hiddenText.setTextColor(Color.RED);
        Toaster(text, this, 3);
    }
    public static void Toaster(String mensaje, Context c, int Tipo) {
        LayoutInflater inflater = LayoutInflater.from(c);
        View layout = inflater.inflate(R.layout.custom_toast, null);
        ImageView image = (ImageView) layout.findViewById(R.id.image);
        if (Tipo==1){
            image.setImageResource(R.drawable.exclamation);
        }
        if (Tipo==2){
            image.setImageResource(R.drawable.alert);
        }
        if (Tipo==3){
            image.setImageResource(R.drawable.error);
        }
        if (Tipo==3){
            image.setImageResource(R.drawable.info);
        }
        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setGravity(Gravity.CENTER);
        text.setText(mensaje);
        Toast toast = new Toast(c);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }
}
