package com.freshmintsoft.inegimapas.service;


import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

import android.util.Log;

import com.freshmintsoft.inegimapas.model.InegiLocal;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * Clase de ayuda, que convierte la respuesta recibida en 
 * formato JSON a un objecto InegiLocal, para su posterior uso
 * Por medio de la libreria de Gson.
 * 
 * Permite convertir un objeto a JSON por medio del metodo
 * <code>convertObjToJson</code>
 * @author freshmintsoft
 * @see https://sites.google.com/site/gson/gson-user-guide
 * 
 */
public class JsonConverter {
	
	private static String TAG = "JSON_CONVERTER";
	private String json;
	Gson gson;
	List<InegiLocal> dataList;
	
	public JsonConverter(String json){
		this.json = json;
		gson = new Gson();
	}
	
	public List<InegiLocal> getListFromJson(){
		Type collectionType = new TypeToken<Collection<InegiLocal>>(){}.getType();
		dataList = gson.fromJson(json, collectionType);
		return dataList;
	}
	
	public String convertObjToJson(Object obj){
		return gson.toJson(obj);
	}
	
	public void logValueOfList(){
		if(dataList != null) {
			for (InegiLocal local : dataList) {
				
				Log.d(TAG, "idDato: " + local.getIdDato() +
							"nombreClase:" + local.getNombreClase());
			}
		} else {
			Log.d(TAG, "List is empty");
		}
	}
}
