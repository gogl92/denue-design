package com.freshmintsoft.inegimapas.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.freshmintsoft.inegimapas.model.GPSUbicacion;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
/**
 * 
 * @author freshmintsoft
 *
 */
public class FusionTablesJsonDeserializer {

	private String json;
	public String kind;
	private Map<String, List<GPSUbicacion>> areaMap;
	
	public FusionTablesJsonDeserializer(String json){
		this.json = json;
		areaMap = new HashMap<String, List<GPSUbicacion>>();
	}
	
	public void deserializeJson(){
		JsonParser parser = new JsonParser();
		JsonElement root = parser.parse(json);
		JsonObject object = root.getAsJsonObject();
		kind = object.get("kind").getAsString();
		
		JsonArray rows = object.get("rows").getAsJsonArray();
		//Outputs: [[{"geometry":{"type":"Polygon","coordinates"...}}]]
		for(int i = 0; i < rows.size(); i ++) {
			List<GPSUbicacion> polygonPath = new ArrayList<GPSUbicacion>();
			JsonObject geometry = rows.get(i).getAsJsonArray().get(0).getAsJsonObject();
			//Outputs : {"geometry":{"type":"Polygon","coordinates":[[[...]]]}}
			JsonArray coordenates = geometry.get("geometry").getAsJsonObject()
							.get("coordinates").getAsJsonArray()
							.get(0).getAsJsonArray();
			//Outputs: [[-102.250942303,21.8628217595,0.0],[-102.251675766,21.8628172686,0.0]...]
			for(int j = 0; j < coordenates.size(); j++){
				JsonArray currentCoordenate = coordenates.get(j).getAsJsonArray();
				//Outputs: [-102.250942303,21.8628217595,0.0]
				String longitud = currentCoordenate.get(0).getAsString();
				String latitud = currentCoordenate.get(1).getAsString();
				String altitud = currentCoordenate.get(2).getAsString();
				GPSUbicacion ubicacion = new GPSUbicacion(longitud, latitud, altitud);
				polygonPath.add(ubicacion);
				
			}
			areaMap.put("Polygon" + i, polygonPath);
		}
	}

	public Map<String, List<GPSUbicacion>> getAreaMap() {
		return areaMap;
	}
	
	
}
