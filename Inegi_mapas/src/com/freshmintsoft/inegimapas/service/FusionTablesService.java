package com.freshmintsoft.inegimapas.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

import com.freshmintsoft.inegimapas.DenueApplication;
import com.freshmintsoft.inegimapas.GoogleMapsListActivity;
import com.freshmintsoft.inegimapas.model.FusionTableArea;
import com.freshmintsoft.inegimapas.model.GPSUbicacion;

import android.content.Context;
import android.os.AsyncTask;

public class FusionTablesService extends
		AsyncTask<GPSUbicacion, Integer, Map<String, List<GPSUbicacion>>> {

	public interface FusionTablesServiceListener{
		public void refreshDialog(Integer progress);
		public void taskFinished();
	}
	
	FusionTablesServiceListener listener;
	GoogleMapsListActivity parent;
	private Context context;
	
	public FusionTablesService(Context context){
		this.context = context;
		parent = (GoogleMapsListActivity) context;
		listener = (FusionTablesServiceListener) context;
	}
	
	@Override
	protected Map<String, List<GPSUbicacion>> doInBackground(GPSUbicacion... arg0) {
		this.publishProgress(new Integer[]{30});
		try {
			String json = loadLocalData();
			this.publishProgress(new Integer[]{60});
			FusionTablesJsonDeserializer deserializer =  new FusionTablesJsonDeserializer(json);
			deserializer.deserializeJson();
			this.publishProgress(new Integer[]{90});
			Map<String, List<GPSUbicacion>> areaMap = deserializer.getAreaMap();
			this.publishProgress(new Integer[]{100});
			return areaMap;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	private String loadLocalData() throws IOException{
		InputStream ins = context.
				getResources().openRawResource(
						com.freshmintsoft.inegimapas.R.raw.cartografia_inegi_parte);
		BufferedReader reader = new BufferedReader(new InputStreamReader(ins));
		String json = "", line = "";
		while((line = reader.readLine()) != null) {
			json += line;
		}
		return json;
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		parent.updateProgressDialog(values[0]);
	}

	@Override
	protected void onPostExecute(Map<String, List<GPSUbicacion>> result) {
		DenueApplication application = (DenueApplication) context.getApplicationContext();
		application.setAreaMap(result);
		application.isMappingPainted = true;
		listener.taskFinished();
	}

	
}
