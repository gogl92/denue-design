package com.freshmintsoft.inegimapas.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class FusionTablesHttpConnection implements Runnable{

	private final String BASE_URL = "https://www.googleapis.com/fusiontables/v1";
	private final String REQUEST_METHOD = "GET";
	private final String HEADERS = "Content-type";
	private String requestType = "text/json; charset=utf-8";
	
	private static final String TABLE_ID = "1_AKNxayzVwRPzCpqWrhuzYVOUpMXnEY4laJNroQ"; //For demo tests.
	private static final String API_KEY = "AIzaSyAGLdPN6MvRTmxXnlKp2OZsw-7RxjZa4o4";
	public boolean isActive = true;
	public String contents = "";
	HttpsURLConnection con;
	InputStream responseStream;
	int limit = 30;
	public FusionTablesHttpConnection() {
		
	}

	public void run(){
		connectToService();
		try {
			readResponseFromServer();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void connectToService(){
		URL fusionTablesUrl;
		try {
			String adds = "/query?sql=SELECT%20ROWID%20FROM%20" + TABLE_ID + "%20LIMIT%20" + limit
					+ "&key=" + API_KEY;
			fusionTablesUrl = new URL(BASE_URL + adds); 
			con = (HttpsURLConnection) fusionTablesUrl.openConnection();
			con.setRequestMethod(REQUEST_METHOD);
			con.setRequestProperty(HEADERS, requestType);
			//con.setDoOutput(true);
			//con.setDoInput(true);
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void readResponseFromServer()throws IOException{
		responseStream = con.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(responseStream));
		String line = "";
		while((line = reader.readLine()) != null){
			contents += line;
		}
		isActive = false;
	}
}
