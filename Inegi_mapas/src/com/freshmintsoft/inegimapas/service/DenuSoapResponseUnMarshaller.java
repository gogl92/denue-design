package com.freshmintsoft.inegimapas.service;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.util.Log;

import com.freshmintsoft.inegimapas.model.InegiLocal;
/**
 * Clase de utilidad que sirva para convertir
 * el stream de informacion del DENUE a su
 * representacion a lista de objetos a traves del
 * metodo<code>unMarshallResponse</code>
 * 
 * @author FreshmintSoft
 * @see InegiLocal
 */
public class DenuSoapResponseUnMarshaller {

	private static String TAG = "DenuSoapResponseUnMarshaller";
	
	private List<InegiLocal> responseList;
	
	public DenuSoapResponseUnMarshaller(){
		responseList = new ArrayList<InegiLocal>();
	}
	/**
	 * Convierte la respuesta del SOAP service a una lista de
	 * objetos tipo InegiLocal.
	 * @param response InputStream generado por el SOAP service.
	 * @return List de tipo InegiLocal
	 * @throws IOException si la respuesta no esta en el formato del DENUE.
	 */
	public List<InegiLocal> unMarshallResponse(InputStream response) throws IOException{
		try {
			Document doc = this.buildDocumentBasedOnResponse(response);
			this.populateListBasedOnDocument(doc);
		} catch (Exception e) {
			Log.e(TAG, "Error creating Document for DENUE response");
		}
		
		return responseList;
	}
	
	private Document buildDocumentBasedOnResponse(InputStream response) throws Exception{
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(response);
		doc.getDocumentElement().normalize();
		
		return doc;
	}
	
	private void populateListBasedOnDocument(Document doc){
		NodeList nList = doc.getElementsByTagName("ArrayOfString");
		
		for(int temp = 0; temp < nList.getLength(); temp++) {
			Node nNode = nList.item(temp);
			if(nNode.getNodeType() == Node.ELEMENT_NODE){
				Element eElement = (Element) nNode;
				InegiLocal local = populateValues(eElement);
				responseList.add(local);
			}
		}
	}

	private InegiLocal populateValues(Element eElement){
		NodeList nlList = eElement.getElementsByTagName("string");
		InegiLocal local = new InegiLocal();
		String[] nodeValues = getNodeValuesAsStringArray(nlList);
		
		
		local.setIdDato( Integer.parseInt( nodeValues[0] ) );
		local.setCodigoPostal(nodeValues[18]);
		local.setAreaGeoestadistica(nodeValues[4]);
		local.setCodigoClase(nodeValues[6]);
		local.setCorredorIndustrial(nodeValues[13]);
		local.setEdificio(nodeValues[14]);
		local.setEmail(nodeValues[20]);
		local.setEntidadFederativa(nodeValues[1]);
		local.setFechaIncorporacion(nodeValues[26]);
		local.setLatitud(nodeValues[24]);
		local.setLocalidad(nodeValues[3]);
		local.setLongitud(nodeValues[25]);
		local.setManzana(nodeValues[5]);
		local.setNombreAsentamiento(nodeValues[12]);
		local.setNombreClase(nodeValues[7]);
		local.setNombreUnidadEconomica(nodeValues[8]);
		local.setNombreVialidad(nodeValues[11]);
		local.setNumeroExterior(nodeValues[15]);
		local.setNumeroInterior(nodeValues[16]);
		local.setNumeroLocal(nodeValues[17]);
		local.setPersonalOcupado(nodeValues[23]);
		local.setRazonSocial(nodeValues[9]);
		local.setTelefono(nodeValues[19]);
		local.setTipoUnidadEconomica(nodeValues[22]);
		local.setTipoVialidad(nodeValues[10]);
		local.setWebsite(nodeValues[21]);
		
		return local;
	}
	
	private String[] getNodeValuesAsStringArray(NodeList nlList){
		String[] values = new String[27];
		Node nValue0 = (Node) nlList.item(0);
		Node nValue1 = (Node) nlList.item(1);
		Node nValue2 = (Node) nlList.item(2);
		Node nValue3 = (Node) nlList.item(3);
		Node nValue4 = (Node) nlList.item(4);
		Node nValue5 = (Node) nlList.item(5);
		Node nValue6 = (Node) nlList.item(6);
		Node nValue7 = (Node) nlList.item(7);
		Node nValue8 = (Node) nlList.item(8);
		Node nValue9 = (Node) nlList.item(9);
		Node nValue10 = (Node) nlList.item(10);
		Node nValue11 = (Node) nlList.item(11);
		Node nValue12 = (Node) nlList.item(12);
		Node nValue13 = (Node) nlList.item(13);
		Node nValue14 = (Node) nlList.item(14);
		Node nValue15 = (Node) nlList.item(15);
		Node nValue16 = (Node) nlList.item(16);
		Node nValue17 = (Node) nlList.item(17);
		Node nValue18 = (Node) nlList.item(18);
		Node nValue19 = (Node) nlList.item(19);
		Node nValue20 = (Node) nlList.item(20);
		Node nValue21 = (Node) nlList.item(21);
		Node nValue22 = (Node) nlList.item(22);
		Node nValue23 = (Node) nlList.item(23);
		Node nValue24 = (Node) nlList.item(24);
		Node nValue25 = (Node) nlList.item(25);
		Node nValue26 = (Node) nlList.item(26);
		
		values[0] = nValue0.getTextContent();
		values[1] = nValue1.getTextContent();
		values[2] = nValue2.getTextContent();
		values[3] = nValue3.getTextContent();
		values[4] = nValue4.getTextContent();
		values[5] = nValue5.getTextContent();
		values[6] = nValue6.getTextContent();
		values[7] = nValue7.getTextContent();
		values[8] = nValue8.getTextContent();
		values[9] = nValue9.getTextContent();
		values[10] = nValue10.getTextContent();
		values[11] = nValue11.getTextContent();
		values[12] = nValue12.getTextContent();
		values[13] = nValue13.getTextContent();
		values[14] = nValue14.getTextContent();
		values[15] = nValue15.getTextContent();
		values[16] = nValue16.getTextContent();
		values[17] = nValue17.getTextContent();
		values[18] = nValue18.getTextContent();
		values[19] = nValue19.getTextContent();
		values[20] = nValue20.getTextContent();
		values[21] = nValue21.getTextContent();
		values[22] = nValue22.getTextContent();
		values[23] = nValue23.getTextContent();
		values[24] = nValue24.getTextContent();
		values[25] = nValue25.getTextContent();
		values[26] = nValue26.getTextContent();
		
		return values;
	}
}
