package com.freshmintsoft.inegimapas.maputils;

import android.graphics.Canvas;

import com.freshmintsoft.inegimapas.DenueApplication;
import com.freshmintsoft.inegimapas.model.GPSUbicacion;
import com.google.android.maps.MapView;

public class MapViewWithZoom extends MapView{
	 int oldZoomLevel = -1;
	 GPSUbicacion ubicacion;
	 DenueApplication app;
	 CircleRadiusOverlay radio = new CircleRadiusOverlay(ubicacion.getLatitud(), 
			   ubicacion.getLongitud(), "radio", this, app.gpsRango);
	 public MapViewWithZoom(android.content.Context context, android.util.AttributeSet attrs) {
	  super(context, attrs);
	 }
	
	 public MapViewWithZoom(android.content.Context context, android.util.AttributeSet attrs, int defStyle) {
	  super(context, attrs, defStyle);
	 }
	
	 public MapViewWithZoom(android.content.Context context, java.lang.String apiKey) {
	  super(context, apiKey);
	 }

	 public void dispatchDraw(Canvas canvas) {
	  super.dispatchDraw(canvas);
	  if (getZoomLevel() != oldZoomLevel) {
	   oldZoomLevel = getZoomLevel();
	   if(oldZoomLevel > 16) {
		   this.getOverlays().add(radio);
	   } else {
		   this.getOverlays().remove(radio);
	   }
	  }
	 }
	 
	 public void setGPSUbicacion(GPSUbicacion ubicacion){
		 this.ubicacion = ubicacion;
	 }
	 
	 public void setApplication(DenueApplication app) {
		 this.app = app;
	 }
}
