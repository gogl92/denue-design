package com.freshmintsoft.inegimapas.maputils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Region.Op;

import com.freshmintsoft.inegimapas.model.GPSUbicacion;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;

/**
 * 
 * @author freshmintsoft
 *
 */
public class PolygonOverlay extends Overlay {

	private Map<String, List<GPSUbicacion>> areaMap;
	private Paint paintFill;
	private Paint paintStroke;
	private static final int ALPHA = 0x30ffffff;
	private static final int COLOR = Color.MAGENTA & ALPHA;
	
	public PolygonOverlay(Map<String, List<GPSUbicacion>> areaMap) {
		this.areaMap = areaMap;
		preparePaints();
	}
	
	private void preparePaints(){
		paintFill = new Paint();
		paintFill.setStyle(Paint.Style.FILL);
		paintFill.setColor(COLOR);
		
		paintStroke = new Paint(Paint.ANTI_ALIAS_FLAG);
		paintStroke.setStyle(Style.STROKE);
		paintStroke.setAntiAlias(true);
		paintStroke.setStrokeWidth(3);
		paintStroke.setColor(COLOR & 0xff7f7f7f);
	}
	
	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {
		super.draw(canvas, mapView, shadow);
		/*if(shadow) {
			return;
		}*/
		Projection projection = mapView.getProjection();
		List<Path> areaPaths = getPaths(projection);
		drawPaths(canvas, areaPaths);
	}

	private List<Path> getPaths(final Projection projection){
		List<Path> areaPaths = new ArrayList<Path>();
		for(Map.Entry<String, List<GPSUbicacion>> entry : areaMap.entrySet()){
			List<GPSUbicacion> sourceList = entry.getValue();
			Path path = new Path();
			path.setFillType(Path.FillType.EVEN_ODD);
			Iterator<GPSUbicacion> it = sourceList.iterator();
			Point point = nextDrawPoint(projection, it);
			path.moveTo(point.x, point.y);
			while (it.hasNext()) {
				point = nextDrawPoint(projection, it);
				path.lineTo(point.x, point.y);
			}
			path.close();
			areaPaths.add(path);
		}
		return areaPaths;
	}
	
	private Point nextDrawPoint(final Projection projection, final Iterator<GPSUbicacion> it){
		GPSUbicacion ub = it.next();
		Point p = new Point();
		projection.toPixels(ub.toGeoPoint(), p);
		return p;
	}
	
	private void drawPaths(final Canvas canvas, final List<Path> areaPaths){
		for(Path path : areaPaths){
			
			canvas.drawPath(path, paintStroke);
			canvas.drawPath(path, paintFill);
			canvas.clipPath(path, Op.DIFFERENCE); //No se dibuje uno sobre otro.
		}
	}
}
