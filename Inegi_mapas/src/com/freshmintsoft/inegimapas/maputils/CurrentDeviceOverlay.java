package com.freshmintsoft.inegimapas.maputils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Paint.Style;

import com.freshmintsoft.inegimapas.R;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;

/**
 * Marca que representa al dispositivo actual en el 
 * mapa de Google Maps.
 * Crea un circulo rojo para representar al dispositivo
 * en el metodo <code>draw</code>
 * @author freshmintsoft
 * @see Overlay
 */
public class CurrentDeviceOverlay extends Overlay
{
	GeoPoint geoPoint;
	String name;
	MapView mapView;
	
	private Paint paintFill;
	private Paint paintStroke;
	private static final int ALPHA = 0x30ffffff;
	private static final int COLOR = Color.MAGENTA & ALPHA;
	int rango;
	int zoom;
	public static int metersToRadius(float meters, MapView map, double latitude) {
	    return (int) (map.getProjection().metersToEquatorPixels(meters) * (1 / Math
	            .cos(Math.toRadians(latitude))));
	}
	
	public CurrentDeviceOverlay(double latitude, double longitude, 
								String name, MapView mapView, int rango){
		Double geoLatitude = latitude * 1E6;
		Double geoLongitude = longitude * 1E6;
		this.rango = rango;
		geoPoint = new GeoPoint(geoLatitude.intValue(), geoLongitude.intValue());
		this.name = name;
		this.mapView = mapView;
		
		prepareCircleAroundLocation();
	}

	private void prepareCircleAroundLocation(){
		paintFill = new Paint();
		paintFill.setStyle(Paint.Style.FILL);
		paintFill.setColor(COLOR);
		
		paintStroke = new Paint(Paint.ANTI_ALIAS_FLAG);
		paintStroke.setStyle(Style.STROKE);
		paintStroke.setAntiAlias(true);
		paintStroke.setStrokeWidth(3);
		paintStroke.setColor(COLOR & 0xff7f7f7f);
	}
	
	@Override
	public void draw(Canvas canvas, MapView mapview, boolean shadow) {
		Projection projection = mapView.getProjection();
		if(shadow == false){
			Point myPoint = new Point();
			projection.toPixels(geoPoint, myPoint);
			
			Bitmap bmp = BitmapFactory.decodeResource(mapview.getResources(), R.drawable.device_map_overlay);
			canvas.drawBitmap(bmp, myPoint.x, myPoint.y, null);
			int radius = (int) mapView.getProjection().metersToEquatorPixels(rango);
			RectF oval = new RectF(myPoint.x - radius, myPoint.y - radius, 
									myPoint.x + radius, myPoint.y + radius);
			
			canvas.drawOval(oval, paintFill);
		}
	}
}