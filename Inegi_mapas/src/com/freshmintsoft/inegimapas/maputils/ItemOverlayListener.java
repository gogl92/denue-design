package com.freshmintsoft.inegimapas.maputils;

public interface ItemOverlayListener {

	public void onOverlayClicked(int overlayIndex);
}
