package com.freshmintsoft.inegimapas;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import com.freshmintsoft.inegimapas.maputils.CurrentDeviceOverlay;
import com.freshmintsoft.inegimapas.maputils.DenueItemOverlay;

import com.freshmintsoft.inegimapas.maputils.ItemOverlayListener;
import com.freshmintsoft.inegimapas.maputils.PolygonOverlay;
import com.freshmintsoft.inegimapas.model.GPSUbicacion;
import com.freshmintsoft.inegimapas.model.InegiLocal;
import com.freshmintsoft.inegimapas.service.FusionTablesService;

import com.google.android.maps.GeoPoint;

import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;

import com.google.android.maps.Overlay;



import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;

import android.graphics.drawable.Drawable;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Representacion en mapa de los resultados obtenidos por el
 * Denue Service.
 * 
 * @author freshmintsoft
 *
 */
public class GoogleMapsListActivity extends MapActivity 
									implements ItemOverlayListener, 
												FusionTablesService.FusionTablesServiceListener{
	MapController mc;
	MapView mapView;
	GeoPoint devicePoint;
	DenueApplication app;
	/** marcas que se muestran en el mapa */
	List<Overlay> listOfOverlays;
	DenueItemOverlay denueOverlays;
	Button inicioButton;
	Button localizar;
	Button cartografia;
	Button satelite;
	Dialog dialog;
	private boolean isMultipleItemsSelected = true;
	private int indexSelected = -1;
	List<InegiLocal> resultados = new ArrayList<InegiLocal>();
	private ProgressDialog progress;
	private static final int PROGRESS_DIALOG = 999;
	
	static final int ZOOM = 16;
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.activity_mapa);
		initializeComponents();	
		toggleMapView();
		setSatelliteViewForStart();
		setButtonsListeners();
		getIfSingleItemSelected();
		createDeviceLocationInMap();
		turnListItemsIntoPoints();
		drawDenueMapping();
		refreshMapView();
		
	}
	
	@Override
	protected void onStart() {
		super.onStart();
	}
	
	@Override
	@Deprecated
	protected Dialog onCreateDialog(int id) {
		switch(id){
		case PROGRESS_DIALOG:
			progress = new ProgressDialog(this);
			//progress.setIcon(R.drawable.downloading_icon);
			progress.setTitle("Obteniendo Cartografia");
			progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			return progress;
		default:
			ResultadosDetalleActivity detailDialog = new ResultadosDetalleActivity(this); 
			detailDialog.initializeComponents();
			InegiLocal local = resultados.get(app.overlayIndexSelected);
			detailDialog.setDataToComponents(local);
			return detailDialog.create();
		}
		
	}
	
	
	/*
	 * (non-Javadoc) No se necesita trazar la ruta.
	 * @see com.google.android.maps.MapActivity#isRouteDisplayed()
	 */
	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
	
	@Override
	public void refreshDialog(Integer progress) {
		// TODO Auto-generated method stub	
	}

	@Override
	public void taskFinished() {
		drawDenueMapping();
		progress.dismiss();
		unlockScreenOrientation();
	}
	
	public void onOverlayClicked(int indexOverlay){
		app.overlayIndexSelected = indexOverlay;
		showDialog(indexOverlay);
	}
	
	private void initializeComponents(){
		mapView = (MapView) findViewById(R.id.mapView);
		//mapView.setBuiltInZoomControls(true);
		
		
		inicioButton = (Button) findViewById(R.id.mapa_refresh_btn);
		localizar = (Button) findViewById(R.id.mapa_locate_btn);
		cartografia = (Button) findViewById(R.id.mapa_fusion_btn);
		satelite = (Button) findViewById(R.id.mapa_toggle_btn);
		
		mc = mapView.getController();
		app = (DenueApplication)getApplication();
		resultados = app.getDenueResultados();
		//mapView.setTraffic(true);
		listOfOverlays = mapView.getOverlays();
		listOfOverlays.clear();
		Drawable marker = getMarkerImage();
		denueOverlays = new DenueItemOverlay(marker, this);
	}
	
	private void setButtonsListeners(){
		inicioButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), BusquedaActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
			}
		});
		localizar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				animateMapToGeoPoint(devicePoint, ZOOM);
			}
		});
		cartografia.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(!app.isMappingPainted) {
					showDialog(PROGRESS_DIALOG);
					lockOrientation();
					progress.setProgress(0);
					FusionTablesService ftTask = new FusionTablesService(GoogleMapsListActivity.this);
					ftTask.execute(new GPSUbicacion[]{});
				}
			}
		});
		satelite.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				toggleMapView();
				refreshMapView();
			}
		});
	}
	
	private void setSatelliteViewForStart(){
		mapView.setSatellite(true);
		mapView.setStreetView(false);
		app.enableStreetView = false;
		satelite.setText("Vista Mapa");
	}
	
	private void toggleMapView(){
		if(app.enableStreetView) {
			mapView.setSatellite(true);
			mapView.setStreetView(false);
			app.enableStreetView = false;
			satelite.setText("Vista Mapa");
		} else {
			mapView.setStreetView(true);
			mapView.setSatellite(false);
			app.enableStreetView = true;
			satelite.setText("Vista Sat�lite");
		}
	}
	public void updateProgressDialog(Integer amount){
		progress.incrementProgressBy((int)(100 / amount));
	}
	
	private void getIfSingleItemSelected(){
		indexSelected = app.detalleIndexSelected;
		if(indexSelected >= 0) {
			isMultipleItemsSelected = false;
		}
	}
	
	private void createDeviceLocationInMap(){
		GPSUbicacion current = getDeviceLocation();
		if(current.getLatitud() != null && current.getLongitud() != null) {
			devicePoint = convertLocationToGeoPoint(current.getLatitud(), current.getLongitud());
			addDeviceLocationToMap(current);
		}
	}
	
	private void turnListItemsIntoPoints() {
		int count = 0;
		for(InegiLocal resultado : resultados) {
			double latitud = Double.parseDouble(resultado.getLatitud());
			double longitud = Double.parseDouble(resultado.getLongitud());
			GeoPoint location = this.convertLocationToGeoPoint(latitud, longitud);
			if(count == 0) {
				animateMapToGeoPoint(location, ZOOM);
				count++;
			}
			denueOverlays.addNewItem(resultado.getIdDato(),
									 location, 
									 resultado.getNombreUnidadEconomica(), 
									 null);
		}
		listOfOverlays.add(denueOverlays);
		animateDenueOverlaySelected();
	}
	
	private void drawDenueMapping(){
		Map<String, List<GPSUbicacion>> result = app.getAreaMap();
		if(result != null){
			PolygonOverlay cart = new PolygonOverlay(result);
			listOfOverlays.add(cart);
			refreshMapView();
			app.isMappingPainted = true;
		}
	}
	
	
	private Drawable getMarkerImage(){
		Drawable marker = getResources().getDrawable(R.drawable.map_overlay_icon);
		return marker;
	}
	
	private void animateDenueOverlaySelected(){
		if(!isMultipleItemsSelected) {
			InegiLocal animateTo = resultados.get(indexSelected);
			centerMapInItem(animateTo);
			denueOverlays.changeMarkerToBlue(indexSelected);
			app.detalleIndexSelected = -1;
		}
	}
	
	private GPSUbicacion getDeviceLocation(){
		GPSUbicacion current = app.getUbicacion();
		return current;
	}
	
	private GeoPoint convertLocationToGeoPoint(Double latitud, Double longitud){
		GeoPoint point = new GeoPoint(
				(int)(latitud * 1E6), 
				(int)(longitud * 1E6));
		return point;
	}
	
	private void centerMapInItem(InegiLocal local){
		Double latitud = Double.valueOf(local.getLatitud());
		Double longitud = Double.valueOf(local.getLongitud());
		
		GeoPoint localPoint = convertLocationToGeoPoint(latitud, longitud);
		animateMapToGeoPoint(localPoint, ZOOM);
	}
	
	private void animateMapToGeoPoint(GeoPoint point, int zoom){
		mc.animateTo(point);
		mc.setZoom(zoom);
	}
	
	private void refreshMapView(){
		mapView.invalidate();
	}
	
	private void addDeviceLocationToMap(GPSUbicacion ubicacion){
		CurrentDeviceOverlay yo  = new CurrentDeviceOverlay(ubicacion.getLatitud(), 
										ubicacion.getLongitud(), "Dispositivo", 
										mapView, app.gpsRango);
		listOfOverlays.add(yo);
		
	}
	 
	private void lockOrientation(){
		int currentOrientation = getResources().getConfiguration().orientation;
		if(currentOrientation == Configuration.ORIENTATION_LANDSCAPE){
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		}
	}
	
	private void unlockScreenOrientation(){
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
	}
}