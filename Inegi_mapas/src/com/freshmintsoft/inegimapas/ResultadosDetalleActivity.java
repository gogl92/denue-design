package com.freshmintsoft.inegimapas;



import com.freshmintsoft.inegimapas.model.InegiLocal;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;

import android.view.View;

import android.widget.TextView;

/**
 * Dialogo representando a los detalles de un componente
 * de la lista de resultados obtenida por el Denue Service.
 * 
 * @author freshmintsoft
 *
 */
public class ResultadosDetalleActivity extends AlertDialog.Builder {

	private TextView id;
	private TextView nombreUnidadEconomica;
	private TextView razonSocial;
	private TextView codigoClase;
	private TextView nombreClase;
	private TextView personalOcupado;
	private TextView tipoVialidad;
	private TextView nombreVialidad;
	private TextView numeroExterior;
	private TextView edificio;
	private TextView numeroInterior;
	private TextView nombreAsentamiento;
	private TextView corredorIndustrial;
	private TextView numeroLocal;
	private TextView codigoPostal;
	private TextView entidadFederativa;
	private TextView localidad;
	private TextView areaGeoestadistica;
	private TextView manzana;
	private TextView telefono;
	private TextView email;
	private TextView website;
	private TextView tipoUnidadEconomica;
	private TextView fechaIncorporacion;
	
	public static final String LIST_INDEX_KEY = "lastIndexKey";
	int index = -1;
	Activity parentActivity;
	private View view;
	Context context;
	
	/** Constructor que se utiliza en la vista de mapa, no muestra el boton de mapa */
	public ResultadosDetalleActivity(Context context){
		super(context);
		this.parentActivity = (Activity)context;
		onCreate();
		setPositiveButton();
		this.context = context;
		
	}
	
	/** Constructor que se utiliza en la vista de Lista de resultados, muestra boton para mostrar el mapa */
	public ResultadosDetalleActivity(Context context, Integer id) {
		super(context);
		index = id;
		this.parentActivity = (Activity)context;
		onCreate();
		setPositiveButton();
		setMapButton();
		this.context = context;
		
	}
	
	public void onCreate() {
		LayoutInflater inflater = parentActivity.getLayoutInflater();
		view = inflater.inflate(R.layout.activity_detalle, null);
		this.setView(view);
	}

	private void setPositiveButton(){
		this.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();	
			}
		});
	}
	
	private void setMapButton(){
		this.setNegativeButton("Mapa", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				centerSelectedDetailInMap();
				
			}
		});
	}
	
	private void centerSelectedDetailInMap(){
		Intent i = new Intent(context, TabMapsListActivity.class);
		context.startActivity(i);
	}
	
	public void initializeComponents(){
		id = (TextView) view.findViewById(R.id.detalle_id);
		nombreUnidadEconomica = (TextView) view.findViewById(R.id.detalle_nombreUnidadEconomica);
		razonSocial = (TextView) view.findViewById(R.id.detalle_razonSocial);
		codigoClase = (TextView) view.findViewById(R.id.detalle_codigoClaseActividad);
		nombreClase = (TextView) view.findViewById(R.id.detalle_nombreClaseActividad);
		personalOcupado = (TextView) view.findViewById(R.id.detalle_personaOcupada);
		tipoVialidad = (TextView) view.findViewById(R.id.detalle_tipoVialidad);
		nombreVialidad = (TextView) view.findViewById(R.id.detalle_nombreVialidad);
		numeroExterior = (TextView) view.findViewById(R.id.detalle_numeroExterior);
		edificio = (TextView) view.findViewById(R.id.detalle_edificioPisoONivel);
		numeroInterior = (TextView) view.findViewById(R.id.detalle_numeroInterior);
		nombreAsentamiento = (TextView) view.findViewById(R.id.detalle_nombreAsentamiento);
		corredorIndustrial = (TextView) view.findViewById(R.id.detalle_corredorIndustrial);
		numeroLocal = (TextView) view.findViewById(R.id.detalle_numeroLocal);
		codigoPostal = (TextView) view.findViewById(R.id.detalle_codigoPostal);
		entidadFederativa = (TextView) view.findViewById(R.id.detalle_entidadFederativa);
		localidad = (TextView) view.findViewById(R.id.detalle_localidad);
		areaGeoestadistica = (TextView) view.findViewById(R.id.detalle_areaGeoEstadistica);
		manzana = (TextView) view.findViewById(R.id.detalle_manzana);
		telefono =(TextView) view.findViewById(R.id.detalle_telefono);
		email = (TextView) view.findViewById(R.id.detalle_email);
		website = (TextView) view.findViewById(R.id.detalle_website);
		tipoUnidadEconomica = (TextView) view.findViewById(R.id.detalle_tipoUnidadEconomica);
		fechaIncorporacion = (TextView) view.findViewById(R.id.detalle_fechaIncorporacion);
	}
	
	public void setDataToComponents(InegiLocal seleccion){	
		if(seleccion != null){
			id.setText(seleccion.getIdDato().toString());
			nombreUnidadEconomica.setText(seleccion.getNombreUnidadEconomica());
			razonSocial.setText(seleccion.getRazonSocial());
			codigoClase.setText(seleccion.getCodigoClase());
			nombreClase.setText(seleccion.getNombreClase());
			personalOcupado.setText(seleccion.getPersonalOcupado());
			tipoVialidad.setText(seleccion.getTipoVialidad());
			nombreVialidad.setText(seleccion.getNombreVialidad());
			numeroExterior.setText(seleccion.getNumeroExterior());
			edificio.setText(seleccion.getEdificio());
			numeroInterior.setText(seleccion.getNumeroInterior());
			nombreAsentamiento.setText(seleccion.getNombreAsentamiento());
			corredorIndustrial.setText(seleccion.getCorredorIndustrial());
			numeroLocal.setText(seleccion.getNumeroLocal());
			codigoPostal.setText(seleccion.getCodigoPostal());
			entidadFederativa.setText(seleccion.getEntidadFederativa());
			localidad.setText(seleccion.getLocalidad());
			areaGeoestadistica.setText(seleccion.getAreaGeoestadistica());
			manzana.setText(seleccion.getManzana());
			telefono.setText(seleccion.getTelefono());
			email.setText(seleccion.getEmail());
			website.setText(seleccion.getWebsite());
			tipoUnidadEconomica.setText(seleccion.getTipoUnidadEconomica());
			fechaIncorporacion.setText(seleccion.getFechaIncorporacion());
		}
	}
}
